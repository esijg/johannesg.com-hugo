+++
title = "{{ replace .TranslationBaseName "-" " " | title }}"
date = {{ .Date }}
draft = true

cv = true
portfolio = true

categories = [
  "games",
  "music",
  "events",
  "education",
  "awards",
  "visual art"
]

image = "/static/img/default-project.jpg"
+++

# Woot

test
