---
title: "About"
date: 2017-07-11T10:07:23Z
draft: false
---
A sound designer, game developer, musician, and all around tech obsessionist based in north Iceland. Makes [noises](http://johannesgthorsteinsson.bandcamp.com "My Bandcamp page") & [games](http://johannesg.itch.io "My itch.io page"). Hosts game developers at the [Kollafoss Gamedev Residency](http://www.kollafoss.famr) and the yearly [Isolation Game Jam](http://kollafoss.farm/isolationgamejam/).
