---
categories: Photography
date: 2012-10-25T00:00:00Z
title: Black Horizon's BH-50 Provocative Rituals Art Zine
url: /2012/10/25/black-horizons-bg-50-provocative-rituals-art-zine/
---

![](/assets/images/blog/2012-10-25_01.jpg "")
Got this one in mail the other day from Black Horizons. Featuring photographs by yours truly along with artwork by other artists I hold great respect for. A great collection of provocative imagery and a perfect conversation starter by the coffee table.

> "Featuring art and writing, all in a theme of violent eroticism, from Josh Lay, [Terence Hannum](http://terencehannum.com/home.html), Jóhannes Gunnar Þorsteinsson, [Kevin Gan Yuen](http://www.viraloptic.com), Komissar Hjuler und Mama Baer, Chris Bickel, Genevieve Larson, Ryan Whittier Hale, [Thomas Ekelund](http://dreadcade.com), and J Hartmann along with lots of [Black Horizons](http://www.black-horizons.com) design. An object of fantastic transgression, not for the faint of heart. Printed on pearl stock, with black metallic card stock cover and OBI, and a black metallic envelope. Limited to 50 copies."

[Order from Black Horizons](http://www.black-horizons.com/order.html)