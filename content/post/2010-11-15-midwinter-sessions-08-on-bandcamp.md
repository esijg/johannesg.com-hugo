---
categories: ziz audio
date: 2010-11-15T00:00:00Z
title: Midwinter Sessions '08 on Bandcamp
url: /2010/11/15/midwinter-sessions-08-on-bandcamp/
---

![](/assets/images/blog/2010-11-15_01.jpg "")
Both the digital and physical version of Midwinter Sessions '08 are now available on [Ziz' bandcamp page](http://voidziz.bandcamp.com/album/midwinter-sessions-08).

Midwinter Sessions '08 was a split between Ziz and Fermentæ (US) done in 2008 and originally released in 2009 and then re-released in 2009 in different packaging. Below is the original description.

	> Four tracks, over 60 minutes of dark-ambient dronescapes, spectral riffing and eerie field-recordings; compiled, captured and composed by **Kevin Gan. Yuen (FERMENTÆ)** and **Jóhannes G. Þorsteinsson (Ziz)**. Hand-assembled and hand-numbered, black-bottom CD-r's, screen-printed with metallic-bronze inks and packaged in resealable poly-sleeves.

Also, keep your eyes open on this site for releases in the near future. Some unreleased material will make its way here soon!