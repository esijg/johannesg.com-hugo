---
categories: S Interactive
date: 2012-10-08T00:00:00Z
title: Sjö has landed
url: /2012/10/08/sjo-has-landed/
---

![](/assets/images/blog/2012-10-08_01.jpg "")
After 3 trips over the Atlantic ocean *(thanks to some shipping complications)* [Sjö](http://johannesg.com/discography/ziz-sj-remastered) has finally arrived in my hands and I can now without any doubt say that [Cameron of Merz Tapes](http://www.merztapes.com), [Kevin Yuen of Viraloptic](http://www.viraloptic.com) and [Bryan W Bray](http://soundcloud.com/cetacea) have done a spectacular job with squeezing the most out of this release of mine both visually, audially and physically.

The soft edged plastic case houses a white C27 cassette and a folded vellum paper J-card displaying beautiful 2 tone artwork by [Kevin Yuen](http://www.viraloptic.com). I can't simply describe how happy I am with how the metallic golden ink plays with the vellum paper. Another extra you won't see on the digital images of the cover is how the backside artwork shines to the front through the semi transparent vellum paper. Giving us a completely different composition and feel than how the design looked like on screen.

#Obtaining it
If you are interested in obtaining a copy your only choice currently is to order it directly from me by contacting me via [e-mail](mailto:johannesg@johannesg.com) as [Sjö](http://johannesg.com/discography/ziz-sj-remastered) has been sold out  at [Merz Tapes](http://www.merztapes.com) as far as I know. There might be some left over at [Aquarius Records](http://www.aquariusrecords.org). I got very limited amount of copies so first come first serve.

Price is 9 USD + shipping.

*PS: for those interested, the other tapes on the photograph were a nice little bonus to me from [Merz Tapes](http://www.merztapes.com). Copies of [Terrence Hannum's Final Salt](http://www.discogs.com/Terence-Hannum-Final-Salt/release/3730359), [Meditative Sect's Laceration Points](http://www.discogs.com/Meditative-Sect-Laceration-Points/release/3714391) and [Oikos' Ecotono](http://www.discogs.com/Oikos-Ecotono/release/3817196). All of them [highly recommended](http://www.merztapes.com/catalog.html).*