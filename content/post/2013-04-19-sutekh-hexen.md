---
categories: Music
date: 2013-04-19T00:00:00Z
title: Sutekh Hexen
url: /2013/04/19/sutekh-hexen/
---

I am forwarding this message from a dear friend, Kevin Yuen, of the band Sutekh Hexen who recently ran into some problems with their music label which decided to rerelease one of their records without their permission. They kindly ask you to not buy this unofficial bootleg pressing of their LP "Luciform" from the record label, Wands.

Following is the official statement from the source:

> It has come to our attention through colleagues that former label, WANDS in Bakersfield, CA, have recently reissued SUTEKH HEXEN's "Luciform" LP for Record Store Day without the band's authorization. We have heard various accounts that "he has had a horrible reputation for doing this."

> ABSOLUTELY DO NOT SUPPORT this "third" pressing of our "Luciform" LP. Observations of odd pressing numbers have already shown their intent to capitalize on our art through Ebay and Discogs. Again: this pressing was not approved by the active band, nor were we contacted in any way before this LP was sent into production.

> The end result of their greed and deception shines the brightest light on individuals who need to fact-check information before they choose to damage their reputations further. With public, personal attacks, an already negative track-record of missing merch shipments, and poor record-label etiquette, it was a conscious effort to sever ties with these misinformed individuals very early on.

> We are not concerned with the ramblings, because they continually perpetuate the immature and negative qualities of a small world. The supporters and people that we work with understand the amount of time and hard work that we dedicate to making things happen.

> In closing: the band has been working on a proper mix and mastering treatment, with superior presentation, for an OFFICIAL release of LUCIFORM later in the year.

> Thank you for reading.

[Sutekhhexen.org](http://www.sutekhhexen.org)