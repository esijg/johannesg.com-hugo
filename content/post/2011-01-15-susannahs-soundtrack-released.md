---
categories: ziz audio
date: 2011-01-15T00:00:00Z
image: /img/blog/2011-01-15_01.jpg
title: Susannah's Soundtrack Released
url: /2011/01/15/susannahs-soundtrack-released/
---

Click [here to download](http://voidziz.bandcamp.com/album/susannahs-soundtrack "Stream and Download Susannah's Soundtrack at the Bandcamp Store").

Soundscapes, recorded around 2008/2009 for an interactive (video game) project that went by the name "Susannah". An experiment in how far one could go by stripping the elements of a video game down to its bare essentials of mechanics, dynamics and aesthetics, while in the meantime, showcasing the importance and the purpose of audial elements in interactive entertainment.

Set in a bleak sterile world of white, where nothing existed except disturbingly pure visuals, the game followed the avatar's journey through the bleak levels of the world, which the soundtrack accompanied, to migrate and merge to the unknown entity which called for him.

The game only reached a prototype stage where it was showcased at an exhibition in Verksmiðjan, Hjalteyri (Iceland) in 2009. The soundtrack was supposed to be released alongside the game, but given the fact that the project was put on the shelf shortly after the prototype was finished the soundtrack started collecting dust. Until now.

Susannah's Soundtrack is released digitally as "pay what you want". In other words, you choose if you want to download it for free, or support my work financially on future projects.