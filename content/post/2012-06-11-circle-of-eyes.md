---
categories: Photography Music
date: 2012-06-11T00:00:00Z
title: Circle of Eyes
url: /2012/06/11/circle-of-eyes/
---

![Circle of Eyes LP, Lower one featuring a photograph by yours truly](/assets/images/blog/2012-06-11_01.jpg "Circle of Eyes LP, Lower one featuring a photograph by yours truly")

Received this baby in the mail the other day.  
I am more than honored by the fact that a photograph by me is featured on the *(lower)* insert of the great band [Circle of Eyes'](http://www.myspace.com/circleofeyes) latest self titled record from [Anti-Matter Records](http://www.anti-matterrecords.com/). This pure audial doom is available from [Anti-Matter Records webstore](http://antimatterrecords.bigcartel.com/product/circle-of-eyes-self-titled-lp) and Streamable in its entirety from [here](http://anti-matterrecords.bandcamp.com/album/circle-of-eyes-self-titled).

I could do a lengthy write-up on this record but Anti-Matter Records own words already do a great job at that below. The only thing I can say personally about this record is that it is more than worth its weight in gold. If you are into the sounds of [Khanate](http://www.plotkinworks.com/khanate/) *(It's even mastered by the one and only [James Plotkin](http://www.plotkinworks.com/))* and SunnO))), this is something for you.

> CIRCLE OF EYES is the ambient doom project from SUTEKH HEXEN's Kevin Gan Yuen and NECRITE's Thrull. This recording also features vocals by James R. from SWAMP WITCH and YOUR ENEMY. 

> CIRCLE OF EYES was conceived in 2008 for the love of resonance and embracing the realization of decay in the intent to record/release records and perform the occasional live performance. The creative core-duo of Kevin Gan Yuen (SUTEKH HEXEN, FERMENTÆ, OGHAM) along with Thrull (BLACK FUCKING CANCER and NECRITE) proceed with CIRCLE OF EYES as an on-going project in the practice of unraveling and challenging the conventional notions of the loud, slow and down-tuned metal traditions of the Bay Area's rich sonic tapestry. Joined by vocalist/collaborator James R. (SWAMP WITCH and YOUR ENEMY) this entity's uncompromising collective's vision is to present an exclusive aural documentation of the unquestionable decay of black metal, drone, and experimental music genres. 

> With three mammoth tracks that spread over 37 minutes, this recording is mastered by [James Plotkin](http://www.plotkinworks.com/) specifically for vinyl and digital release. The artwork and design was executed by Kevin Gan Yuen with help from Stephen Wilson for an 18” x 24” fold-out poster and Icelandic (by way of Sweden) artist and musician Jóhannes Gunnar Þorsteinsson (ZIZ) for the insert photography. The jackets are a tip-on style, single sleeve jacket printed from Stoughton Printing Company, black dust sleeves, a three button set that’s housed in a black zip lock bag, and a download card printed on 32 lb. metallic paper to redeem the entire recording which also includes the re-mastered 2009 demo as an added bonus. With everything included, this is 90:06 of drone madness... test your psyche! 

> Edition run of 325 copies in three different color variations: 160 on black vinyl, 110 on white vinyl, and 55 on orange vinyl.

[Be sure to give it a spin](http://anti-matterrecords.bandcamp.com/album/circle-of-eyes-self-titled).