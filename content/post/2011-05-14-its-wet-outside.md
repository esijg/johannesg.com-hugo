---
categories: photography
date: 2011-05-14T00:00:00Z
title: It's Wet Outside
url: /2011/05/14/its-wet-outside/
---

Couple of years ago, in 2009 to be more exact, the sun hid behind the clouds, who used their chance to gently rinse the earth with raindrops throughout the day. Visually, rain is an amazing thing which brings out completely different light both from the atmosphere and the objects on the ground. It darkens the sky and evens it out into a single balanced hue of gray, while the ground sucks in the water and intensifies its colors. Green becomes greener and black becomes blacker. I went outside, though not far, to photograph the colors and light of my favorite weather and the result was the photo I obviously named "It's Wet Outside". A photo showcasing the raindrops collecting on a metal bar, slowly gathering enough weight to be able to make it to the ground and provide nutrition into the soil. What I did not expect was that couple of months later I revisited that photo as I was looking for desktop pictures for my computer. I cropped the metal bar away, only showcasing the background, the bokeh of the landscape that hid behind the metal bar. Little did I know that I would use this desktop picture for the next 3 years, sometimes trying to change to new ones but usually after a short period, always returning back to "It's Wet Outside".

![](/assets/images/blog/2011-05-14_01.jpg "")

I've long wondered what it is about this picture that makes it work so well for me. Maybe it's the perfect balance between foreground and background, subtle enough to keep me undistracted but with enough color & compositional tension to "tickle" my senses. I've tried recreating this photograph in various other conditions but every try seems to be failure and I always return to this good old photograph. It seemed to me that it would be a good idea to start this blog by sharing this desktop picture with you, the reader. Hoping that there are some like-minded individuals out there that might enjoy this image as much as I've done for the past 3 years or so.
