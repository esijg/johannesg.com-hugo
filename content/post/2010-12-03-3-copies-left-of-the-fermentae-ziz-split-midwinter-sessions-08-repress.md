---
categories: ziz audio
date: 2010-12-03T00:00:00Z
title: 3 copies left of the Fermentæ/Ziz split, Midwinter Sessions '08 (Repress)
url: /2010/12/03/3-copies-left-of-the-fermentae-ziz-split-midwinter-sessions-08-repress/
---

Recorded, edited and arranged during the winter solstice of 2008, and originally released in March 2009. Because of growing interest in this release we have decided to re-release this realization of two contrasting-perspectives on the midwinter concept from the foggy and winding hills of San Francisco to the bleak and spacious highlands of Iceland.

Four tracks, over 60 minutes of dark-ambient dronescapes, spectral riffing and eerie field-recordings; compiled, captured and composed by Kevin Gan. Yuen (FERMENTÆ) and Jóhannes G. Þorsteinsson (Ziz). Hand-assembled and hand-numbered, black-bottom CD-r's, screen-printed with metallic-bronze inks and packaged in resealable poly-sleeves.

Grab them at the [store](http://voidziz.bandcamp.com/album/midwinter-sessions-08) before they are gone  (digital purchase also available for $4) or contact me directly at johannesg@johannesg.com