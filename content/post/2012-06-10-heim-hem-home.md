---
categories: null
date: 2012-06-10T00:00:00Z
title: Heim / Hem / Home
url: /2012/06/10/heim-hem-home/
---

![Heim](/assets/images/blog/2012-06-10_01.jpg "Heim")

I have arrived.  
Up in the highlands once again where nothingness meets everything.  
Home.

*(This evenings playlist consists of [Sigurrós' Valtari](http://sigur-ros.co.uk/valtari/videos/varud-inga/) along with [Circle of Eyes' new self titled record](http://anti-matterrecords.bandcamp.com/album/circle-of-eyes-self-titled))*

**PS: Takk [Þórgunnur](http://www.thorgunnur.com) fyrir póstkortið.**