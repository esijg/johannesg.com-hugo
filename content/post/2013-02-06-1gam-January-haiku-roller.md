---
categories: Interactive 1GAM
date: 2013-02-06T00:00:00Z
image: /img/blog/2013-02-06_01.png
title: 1GAM January, Haiku Roller
url: /2013/02/06/1gam-January-haiku-roller/
---

![Screenshot from Haiku Roller](/assets/images/blog/2013-02-06_01.png "Screenshot from Haiku Roller")

I am currently in the middle of research work for my thesis which explains my silence lately. I was planning to write up a post-mortem concerning my first entry for the [One Game a Month challenge](http://onegameamonth.com/) *(which is about releasing one game a month throughout 2013)* but my thesis work has not allowed me to do so.

Instead I will be linking to the excellent [post mortem](http://kylehalladay.com/1gam-january-lessons-learned/) written by [Kyle Halladay](http://kylehalladay.com) who is behind the majority of the work behind the January entry, [Haiku Roller](http://dl.dropbox.com/u/6128167/HaikuRacer.zip "Download the game for mac"). He is my partner in crime throughout at least the first months of One Game a Month *(and hopefully the rest of the challenge!)* and he did amazing job with the first game and I am looking forward to what we will be able to put together in february. *(Spoiler: theme word is **"Sound"**)*
