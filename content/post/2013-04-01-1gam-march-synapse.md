---
categories: Interactive 1GAM
date: 2013-04-01T00:00:00Z
title: 1GAM March, Synapse
url: /2013/04/01/1gam-march-synapse/
---

![The Synapse Logo](/assets/images/blog/2013-04-01_01.png "Synapse Logo")
<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F85668653&amp;color=ff6600&amp;auto_play=false&amp;show_artwork=false"></iframe>
For the third installment of the "One Game a Month" Challenge, I bring you Synapse! It is probably the most interesting [#1GAM](http://www.onegameamonth.com) project I've participated in so far from my perspective. It is purely a tech experiment so I can't promise the playing experience will be enjoyable. To be honest, some of the sounds are horrible for a reason. This project was our first try at implementing and using procedural sounds through Pure Data and libpd which resulted in a lot of weird sounds. [Kyle Halladay](http://www.kylehalladay.com) went overboard with the shaders and I went overboard with the procedural sounds. Although, the sample displayed above is purely sample based and implemented in the interactive experience the old fashioned way.

WASD to walk, Space to Jump, Collect Power Sources (black cubes) and bring them to synapses to progress. Sound is a bit abrasive so it may be best to start with your volume low (for the sake of your speakers).

- [Synapse for Mac OS X](/files/projects/1gam/3_synapse_mac.zip)  
- [Synapse for Windows](/files/projects/1gam/3_synapse_win.zip)  

*PS: We tried making a Linux version but it didn't seem to like our procedural sounds.*

![Screenshot](/assets/images/blog/2013-04-01_02.png "Screenshot")

![Screenshot](/assets/images/blog/2013-04-01_03.png "Screenshot")
