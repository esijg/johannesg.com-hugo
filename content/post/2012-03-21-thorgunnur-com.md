---
categories: Web Design
date: 2012-03-21T00:00:00Z
title: Thorgunnur.com
url: /2012/03/21/thorgunnur-com/
---

![screenshot of thorgunnur.com](/assets/images/blog/2012-03-21_01.png "thorgunnur.com web design")

Couple of months ago I finished a web design for an amazing photographer and a good friend. Þórgunnur Þórsdóttir is an artist that I hold unimaginable amount of respect for and it was simply an honor to get the chance to design her presence on the web.

She has captured the live moments of great unorthodox musicians, along with the spirit of social gatherings in such a way that I am easily immersed in the atmosphere and the mood of the photographs without even knowing the people, nor the occasion portrayed. And that's all just the tip of the iceberg.

[http://www.thorgunnur.com](http://www.thorgunnur.com)
