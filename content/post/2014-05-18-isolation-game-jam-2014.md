---
categories: Interactive Gamejam
date: 2014-05-18T00:00:00Z
image: /assets/images/blog/2014-05-18_06.jpg
title: Isolation Game Jam 2014
url: /2014/05/18/isolation-game-jam-2014/
---

![](/assets/images/blog/2014-05-18_06.jpg)

On the 25th of April a group of people from all over the world appeared on my doorsteps, with bags only holding clothes and computers. Ready to make interactive experiences at the [Isolation Game Jam](http://www.leikjasamsudan.is/isolationgamejam) at Kollafoss. It was *(most likely)* the first ever game jam to be hosted in north Iceland, and to be honest, probably the first ever game jam to be hosted outside Reykjavík. It was also the first event by the newly formed organization [Leikjasamsuðan](http://www.leikjasamsudan.is).

It wasn't until [Kyle Halladay](https://www.twitter.com/khalladay) from Canada arrived first on the farm I realized that this was really happening. I don't know how but I somehow managed to trick 7 people to travel over half the atlantic ocean *(from both sides)* to make games on an old abandoned farm almost up in the highlands of Iceland. For those that are not familiar with the topology of Iceland. There are only settlements around the coastline of Iceland as the center is pretty much cold and inhospitable wasteland. I live close to the so called highland borders. Oh, and did I mention that the farm we used, Kollafoss, has no internet connection and barely any cell phone reception at all?

[Kyle Halladay](https://www.twitter.com/khalladay) traveled from Canada. The Romanian [Horatiu Roman](https://twitter.com/horatiu665) traveled from his current outpost in Denmark. My old Gamedev classmates and friends [Karl Bergendahl](https://www.twitter.com/karlbergendahl), [Karl Lorant](https://www.twitter.com/karllorant) and Minnamari Helmisaari traveled from Sweden. Ágúst Karlsson and [Ben Mathis](https://twitter.com/mr_chompers) of [Snjohus Software](https://twitter.com/SnjohusGames) took a quick ride from Reykjavík and brought their bunny mascot with them and a bowl of candy. (Snjohus Software was therefor the surprise sponsor of [the Isolation Game Jam](http://www.leikjasamsudan.is/isolationgamejam)). Although with so few people we still maxed out the space available and in retrospect I feel like this was just the right amount of people.

After a lot of chatting two themes were chosen, "Electric Forest" and "Lost in Body Space", although those were picked for inspirational purposes only. In hindsight I would think those would had not been needed. [Horatiu Roman](https://twitter.com/horatiu665) pointed it out to me that the landscape would had been enough of an inspiration. Perhaps that's an idea to experiment with next year. Some beverages, some good clothes, and an evening on top of a nearby long dead volcano lavatap called Borgarvirki. **(the panorama above is taken by Horatiu on top of said lavatap)**. Or perhaps a relaxing bath in the natural hot spring of Hveraborgir up in the highlands?

The schedule of each day went something like this: We woke up in the morning, had breakfast, took a little walk around the area, had some lunch, and then sat down and started working. At 18:00 we usually stopped and prepared a feast which usually resulted in us sitting by the table afterwards for hours chatting. In the evenings we did some gamedev work but it usually spiraled into chatting, drinking and screaming at each other as we impaled our computer avatars with arrows in [Towerfall](http://www.towerfall-game.com/).

![](/assets/images/blog/2014-05-18_01.jpg)

## Dreampipes
Ágúst and Ben made Dreampipes for phones/tablets with Karl Bergendahl and myself contributing sound effects to the game. It is not available online yet but something tells me that if they decide to finish it, it will pop up at [Snjohus](http://www.snjohus.is).  
[[Click here for a screenshot](/assets/images/blog/2014-05-18_03.jpg)]

## Abstract Forest
Minnamari Helmisaari made a really interesting visual experience which she called Abstract Forest with sound effects from Karl Bergendahl.  
[[Click here for a screenshot](/assets/images/blog/2014-05-18_05.jpg)]

## Active Mike
Kyle Halladay built Active Mike, a game about shouting at your computer with sound from Karl Bergendahl, and visual contributions from Minnamari Helmisaari and Karl Lorant. There's a build up on [Karl Lorant's blog](http://karllorant.blogspot.se/2014/05/isolation-game-jam-2014.html).  
[[Click here for a screenshot](/assets/images/blog/2014-05-18_02.jpg)]

## Sheepy
Horatiu Roman was inspired by all the sheep that he made a game about sheep herding the Icelandic way. With explosives obviously. Ben, Karl Bergendahl and Minnamari Helmisaari contributed to that game, Sheepy. The plan is to release it on Google Playstore in the near future.  
[[Click here for a screenshot](/assets/images/blog/2014-05-18_04.jpg)]