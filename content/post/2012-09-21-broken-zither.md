---
categories: Audio
date: 2012-09-21T00:00:00Z
title: Broken Zither
url: /2012/09/21/broken-zither/
---

![](/assets/images/blog/2012-09-21_01.jpg "")

Sometimes, you get lucky.  
**Audio-fetish dilemma**  
Repair it or enjoy the broken sound?