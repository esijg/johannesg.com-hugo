---
categories: Interactive 1GAM
date: 2013-03-01T00:00:00Z
title: 1GAM February, Orbs
url: /2013/03/01/1gam-february-orbs/
---

![Screenshot from Orbs](/assets/images/blog/2013-03-01_01.png "Screenshot from Orbs")

[Download it for Mac!](/files/projects/1gam/2_orbs.zip)  
[Download it for Windows!](/files/projects/1gam/2_orbs_win_x86.zip)  
[Download it for Linux!](/files/projects/1gam/2_orbs_linux_universal.zip)

The game for February's [One Game A Month challenge](http://www.onegameamonth.com) has been *"finished"*. It's not a very practical game for you as a gamer as it's more of a proof of concept, or a sketch perhaps. So if you decide to download it and play it, beware!

It was quite a trip though, and from the programming side of things you can read more about it from [Kyle Halladay's blog](http://kylehalladay.com/1gam-february-wrap-up-failure-is-good-right/).

The idea was to make a basic first person puzzle game where the player would walk around with a recorder and "steal" audio sources and use those to power various items for clues or to progress in the game. For the audio part we were going to use [Pure Data](http://www.puredata.info) *([libpd](http://libpd.cc) to be more exact)* to drive the sound behind the game. This would had allowed me to play around with real time effects and procedural audio, but sadly, we ran into some problems with integrating [Pure Data](http://www.puredata.info) into the game engine so in the end, the good old sound sample method was used. [Pure Data](http://www.puredata.info) integration is still on the map though for this (March) month. We are going to keep on hitting that wall until we get through, and for March we will be revisiting the idea of Orbs. Like I said, the February game was more of a sketch, a preperation for March.
 
<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F81322818&amp;color=ff6600&amp;auto_play=false&amp;show_artwork=false"></iframe>

Therefor, there's nothing mindblowing to be said concerning the sound apart from the fact that I purely used the DAW [Reaper](http://www.reaper.fm). I have been slowly switching out programs for a more cross-platform programs to prepare for a possible move to Linux in the near future, and for a DAW, Reaper has been my weapon of choice (even though there is no official Linux version, but it is supposed to run smoothly under Wine).

*PS: Did I tell you how much I love One Game a Month, If you want to get your hands dirty in game design, I would highly recommend joining in on the fun. Go make one horrible game a month.*