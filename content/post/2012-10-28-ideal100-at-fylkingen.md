---
categories: Photography
date: 2012-10-28T00:00:00Z
title: Ideal100 at Fylkingen
url: /2012/10/28/ideal100-at-fylkingen/
---

![](/assets/images/blog/2012-10-28_01.jpg "")
A weekend in Stockholm.  
iDeal100, Trepaneringsritualen, Fylkingen and Bag full of cassettes, cd's and records.  
Slept in a red boat and repeatedly referred to as "the Icelander".  
Woke up to the sight of a man rowing past my bedroom window in a kayak.  
Satisfied. 

![alt text](/assets/images/blog/2012-10-28_02.jpg "title text")
![alt text](/assets/images/blog/2012-10-28_03.jpg "title text")
![alt text](/assets/images/blog/2012-10-28_04.jpg "title text")
![alt text](/assets/images/blog/2012-10-28_05.jpg "title text")
![alt text](/assets/images/blog/2012-10-28_06.jpg "title text")
![alt text](/assets/images/blog/2012-10-28_07.jpg "title text")
![alt text](/assets/images/blog/2012-10-28_08.jpg "title text")
![alt text](/assets/images/blog/2012-10-28_09.jpg "title text")
![alt text](/assets/images/blog/2012-10-28_10.jpg "title text")
![alt text](/assets/images/blog/2012-10-28_11.jpg "title text")
![alt text](/assets/images/blog/2012-10-28_12.jpg "title text")
![alt text](/assets/images/blog/2012-10-28_13.jpg "title text")
![alt text](/assets/images/blog/2012-10-28_14.jpg "title text")
![alt text](/assets/images/blog/2012-10-28_15.jpg "title text")
![alt text](/assets/images/blog/2012-10-28_16.jpg "title text")
![alt text](/assets/images/blog/2012-10-28_17.jpg "title text")
![alt text](/assets/images/blog/2012-10-28_18.jpg "title text")
![alt text](/assets/images/blog/2012-10-28_19.jpg "title text")
![alt text](/assets/images/blog/2012-10-28_20.jpg "title text")
![alt text](/assets/images/blog/2012-10-28_21.jpg "title text")
![alt text](/assets/images/blog/2012-10-28_22.jpg "title text")