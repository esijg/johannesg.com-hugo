---
categories: ziz audio
date: 2010-07-27T00:00:00Z
title: Spirals tracklist
url: /2010/07/27/spirals-tracklist/
---

![](/assets/images/blog/2010-07-27_01.jpg "")

After couple of months of work I'm finally seeing the end of this journey. My upcoming album "Spirals" is now almost ready so I've decided that it's time to reveal the tracklist.

This journey did take some twists and turns I did not anticipate and it ended up as a totally different beast than I originally planned. I do not consider that a bad thing though but I will go through more details on that all when the time comes and the album is ready to be released, which should be in the coming weeks.

1. The Ode that was Lost
2. Rain, Beautiful Blackout
3. Distance, No Hope
4. Prey on the Neurotic's Mind
5. The Curator
6. As the Bones Heal
7. Birth
8. Oneiros' Invitation
9. Talisman
10. Equinox
11. Svartur
12. Spirals
