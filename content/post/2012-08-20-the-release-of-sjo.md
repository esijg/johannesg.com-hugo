---
categories: Audio Ziz
date: 2012-08-20T00:00:00Z
title: The release of Sjö
url: /2012/08/20/the-release-of-sjo/
---

![Ziz - Sjö](/assets/images/blog/2012-08-20_01.jpg "Ziz - Sjö")

<iframe width="300" height="100" style="position: relative; display: block; width: 300px; height: 100px;" src="http://bandcamp.com/EmbeddedPlayer/v=2/album=99323375/size=grande/bgcol=484848/linkcol=e3e3e3/" allowtransparency="true" frameborder="0"><a href="http://voidziz.bandcamp.com/album/sj-remastered">Sjö (Remastered) by Ziz</a></iframe>

**Seven  
sketches or  
soundtracks,  
A revival  
Recorded one  
piece per day.**  

I can't describe how happy I am with this [release](http://johannesg.com/discography/ziz-sj-remastered). Cameron of [Merz Tapes](http://www.merztapes.com) has done a wonderful job at looking over this project and making sure to portray this release in such high quality which I wouldn't had even managed to imagine when I first wrote these songs and released in a mere 7 copies.  

![Ziz - Sjö](/assets/images/blog/2012-08-20_02.jpg)

I would like to thank Cameron of [Merz Tapes](http://www.merztapes.com) for his amazing work and for giving me a chance, [Kevin G. Yuen](http://www.viraloptic.com) for the design (and for simply being there for me), [Bryan W Bray](http://soundcloud.com/cetacea) for adding such clarity to the songs, and [Jonathan G. Irons](http://jonirons.net) for the beautiful prose included on the J-Card. His way with words sometimes makes me want to hire him to do all of my writing from now on.

![Ziz - Sjö](/assets/images/blog/2012-08-20_03.jpg)

**[Download/Order from Bandcamp](http://voidziz.bandcamp.com/album/sj-remastered)**.  
~~C27 Cassettes available from [Merztapes](http://www.merztapes.com/catalog.html)~~  
If you live in Iceland/Sweden (or neighboring countries) and want to save on shipping fees, [contact me directly](mailto:johannesg@johannesg.com) as I will possibly have some spare copies in the near future.
