---
categories: Audio Ziz
date: 2012-08-05T00:00:00Z
image: /img/blog/2012-08-05_01.jpg
imagealt: Ziz - Sjö
imagetitle: Ziz - Sjö
title: Ziz - Sjö, It's getting closer
url: /2012/08/05/ziz-sjo-its-getting-closer/
---

![Ziz - Sjö](/assets/images/blog/2012-08-05_01.jpg "Ziz - Sjö")
Photo by [Kevin G. Yuen](http://www.viraloptic.com).