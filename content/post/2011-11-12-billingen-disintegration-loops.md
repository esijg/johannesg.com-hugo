---
categories: Photography
date: 2011-11-12T00:00:00Z
title: Billingen + Disintegration Loops
url: /2011/11/12/billingen-disintegration-loops/
---

![](/assets/images/blog/2011-11-12_01.jpg "")

Post processed some photos from this morning's walk under the influence of [William Basinski's Disintegration Loops](http://www.discogs.com/William-Basinski-The-Disintegration-Loops-I/release/75994). ([youtube link](http://www.youtube.com/watch?v=5dQweKIHQP0&feature=related))