---
date: 2013-01-02T00:00:00Z
title: Coffee Chess for New-Year Game Jam
url: /2013/01/02/coffee-chess-for-new-year-game-jam/
---

![Coffee Chess](/assets/images/blog/2013-01-02_01.jpg "Coffee Chess")
During New Years Eve a frightening storm was hitting the west and north coast of Iceland which resulted in the best idea for the evening was to stay inside and take it easy. Thankfully, I still had electricity so I decided to participate in [New-Year Game Jam](http://nygj.blechi.at "Folis' New-Year Game Jam's Website") which forces people to create a game in under 48 hours.

After some brainstorming coffee started to infiltrate my game idea and before I knew it I had my core idea fleshed out. "A boardgame about coffee, made from coffee"

I wanted to keep things simple so you don't need to print out a game board or anything. The only thing you need are a regular 8x8 chess board and 50 coffee beans or other similar items. Coffee beans are recommended as they make your hands smell wonderful after the game.

[Download the NYGJ version of the Coffee Chess rules here](/sites/default/files/blog/Coffee%20Chess.zip "a ZIP file including a PDF").
~~[Download mirror from nygj.blechi.at](http://nygj.blechi.at/games/hosted/Coffee%20Chess.zip "a ZIP file including a PDF from nygj.blechi.at").~~
[The source/latest version from gitlab](https://gitlab.com/johannesg/Coffee-Chess).  
~~Try out all the other New Years Game Jam games over [here](http://nygj.blechi.at/games)~~.  

Feel free to contact me at johannesg@johannesg.com if you have any suggestions, feedback or tips concerning this board game.
