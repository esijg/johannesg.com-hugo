---
categories: S Interactive Ziz
date: 2012-11-22T00:00:00Z
title: Tuning the Instruments for S
url: /2012/11/22/tuning-the-instruments-for-s/
---

![](/assets/images/blog/2012-11-22_01.jpg "")
Slowly tuning all of my instruments to prepare them for a recording session for [S](http://www.johannesg.com/blog/s "All S blogposts on this blog"). Well, at least all of the instruments that were tune-able.

I realized the other day that some people probably don't know much about what [S](http://www.johannesg.com/blog/s "All S blogposts on this blog") is all about so I am considering doing a long writeup on what this project revolves around, and for whom I am making it. All I will tell you right now is that if you do like my music, and like making noises yourself. Then [S](http://www.johannesg.com/blog/s "All S blogposts on this blog") might be for you. Imagine the ability to be thrown into the world behind the music, and have the ability to affect the music and make it your own.

Think about it as an interactive [Ziz](http://www.johannesg.com/blog/ziz "Ziz blogposts") release. A [Ziz](http://www.johannesg.com/blog/ziz "Ziz blogposts") "Choose your own adventure" release, or to be more precise, "Choose your own noise".