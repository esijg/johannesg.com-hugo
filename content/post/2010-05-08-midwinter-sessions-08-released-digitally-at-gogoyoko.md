---
categories: ziz audio
date: 2010-05-08T00:00:00Z
title: Midwinter Sessions '08 released digitally at Gogoyoko
url: /2010/05/08/midwinter-sessions-08-released-digitally-at-gogoyoko/
---

![Midwinter Sessions '08 at Gogoyoko](/assets/images/blog/2010-05-08_01.gif "Midwinter Sessions '08 at Gogoyoko")
Midwinter Sessions '08 has now been released digitally at Gogoyoko.com for a mere €2.70. If the Gogoyoko music store is not available in your region then let me know at johannesgunnar@gmail.com and I'll get you an invite.

~~[Midwinter Sessions '08 at Gogoyoko](http://www.gogoyoko.com/album/Midwinter_Sessions_08).~~

**Update: Gogoyoko no longer exists. You can get Midwinter Sessions '08 at [Bandcamp](https://voidziz.bandcamp.com/album/midwinter-sessions-08).** 
