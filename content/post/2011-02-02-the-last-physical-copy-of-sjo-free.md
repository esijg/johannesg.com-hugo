---
categories: ziz audio
date: 2011-02-02T00:00:00Z
title: The Last Physical Copy of Sjö, free.
url: /2011/02/02/the-last-physical-copy-of-sjo-free/
---

![](/assets/images/blog/2011-02-02_01.png "")

Here in my hands, I have the last available copy of Sjö, (limited edition of 7) and I've decided that the first person to e-mail me at johannesg@johannesg.com, claiming the copy, will receive it free of charge. If for some reasons no e-mail will be received by the end of this week (sunday, midnight GMT) then it will be included in a random order on any other release of mine in the near future.

To refresh your memories, here's the description of the release.

	Seven sketches, conceived, composed and recorded with the limitation of one song per day. An experiment to get back into recording after quite a trying move to another country. 

	To accompany the digital release, a physical cassette with hand-painted paper sleeve (each sleeve being unique) was made in a Limited run of 7 copies.
	released 24 December 2010

(http://voidziz.bandcamp.com/album/sj)[http://voidziz.bandcamp.com/album/sj]