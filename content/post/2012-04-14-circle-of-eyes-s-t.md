---
categories: Audio Photograph
date: 2012-04-14T00:00:00Z
title: Circle of Eyes S/T
url: /2012/04/14/circle-of-eyes-s-t/
---

![Circle of Eyes Assembly](/assets/images/blog/2012-04-14_01.jpg "Circle of Eyes Assembly")

[Kevin Yuen](http://www.viraloptic.com) (Circle of Eyes, Sutekh Hexen) posted this photograph online couple of days ago documenting the assembling of the latest release of the Circle of Eyes self titled LP. This is most likely the first time a photograph by me is featured on the packaging on this graceful format. It can be spotted on the lower part of the attached picture, on the paper sleeves sitting on top of the cardboard. Needless to say, I am overjoyed, and I can't wait to get this audial spectacle in my hands.

The LP is available from Anti-Matter records over at [http://antimatterrecords.bigcartel.com/](http://antimatterrecords.bigcartel.com/)