---
categories: ziz audio
date: 2010-08-13T00:00:00Z
title: Releasing the Spirals
url: /2010/08/13/releasing-the-spirals/
---

![](/assets/images/blog/2010-08-13_01.jpg "")

**[Give it a listen or order it online](http://voidziz.bandcamp.com/album/spirals)**

Originally, an ode to the youth, the family, the farm, the highlands and the life we feed on. But saturated by an unexpected paranoia. An ode gone wrong...

Over 70 minutes composed/summoned during the first two quarters of 2010 with field recordings and guitars playing the lead role in the whole composition.

Black CDr in a plastic sleeve in a homemade felted wool pouch. CD paper insert with original artwork by yours truly and text typewritten with an antique typewriter.

1. The Ode that was Lost
2. Rain, Beautiful Blackout
3. Distance, No Hope
4. Prey on the Neurotic's Mind
5. The Curator
6. As the Bones Heal
7. Birth
8. Oneiros' Invitation
9. Talisman
10. Equinox
11. Svartur
12. Spirals

Order through [bandcamp](http://voidziz.bandcamp.com/album/spirals) or contact me directly via E-mail at [johannesg@johannesg.com](mailto:johannesg@johannesg.com)

If you live in Iceland, contact me directly via e-mail so the purchase won't need to go through paypal.
