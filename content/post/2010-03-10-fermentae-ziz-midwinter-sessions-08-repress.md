---
categories: ziz audio
date: 2010-03-10T00:00:00Z
title: FERMENTÆ / ZIZ - 'Midwinter Sessions '08' (Repress)
url: /2010/03/10/fermentae-ziz-midwinter-sessions-08-repress/
---

![Midwinter Sessions '08](/assets/images/blog/2010-03-10_01.png "Midwinter Sessions '08")

Recorded, edited and arranged during the winter solstice of 2008, and originally released in March 2009. Because of growing interest in this release we have decided to re-release this realization of two contrasting-perspectives on the midwinter concept from the foggy and winding hills of San Francisco to the bleak and spacious highlands of Iceland.

Four tracks, over 60 minutes of dark-ambient dronescapes, spectral riffing and eerie field-recordings; compiled, captured and composed by **Kevin Gan. Yuen (FERMENTÆ)** and **Jóhannes G. Þorsteinsson (ZIZ)**. Hand-assembled and hand-numbered, black-bottom CD-r's, screen-printed with metallic-bronze inks and packaged in resealable poly-sleeves.

**Pricing info:**
*$9 (shipping included) for North America. (contact kevin@viraloptic.com) 1000 ISK (shipping included) for Iceland & €7 (shipping included) for the rest of Europe. (contact johannesgunnar@gmail.com)*