---
categories: Interactive
date: 2012-02-01T00:00:00Z
title: Oroborosourous
url: /2012/02/01/oroborosourous/
---

![](/assets/images/blog/2012-02-01_01.png "")

Last weekend I participated in the Global Game Jam (Skövde, SE) which is a challenge where you are supposed to make a video game in only 48 hours with the theme of [Ouroboros](http://en.wikipedia.org/wiki/Ouroboros). Needless to say, it was one of the most hectic weekends I've had in a while where sleep was put on lowest priority. The results *(Achieved by the help of four other people, Minnamari Helmisaari, Jacob Torrång, Becky Ferm & Stina Fernlund)* was the following relaxing game where you control a jelly fish trying to enrichen and balance the sea eco-system by hypnotizing, converting or killing other fishes. Quite proud of this given the tight timeframe.

![Oroborosourous](/assets/images/blog/2012-02-01_02.jpg)

[Global Game Jam Oroborosourous page](http://globalgamejam.org/2012/ourobourosouros).
