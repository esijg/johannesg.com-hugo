+++
title = "Susannah"
date = 2013-10-19T00:00:00Z
draft = false

cv = true
portfolio = true

categories = [
  "games",
  "music",
  "events",
  "education",
  "awards",
  "visual art"
]

image = "/static/img/default-project.jpg"
+++


For Windows/OSX (and Linux if you want to jump through a couple of hoops). An experiment in how far one can go by stripping the elements of a video game down to its bare essentials of mechanics, dynamics and aesthetics, while in the meantime, showcasing the importance and the purpose of audial elements in interactive entertainment. (Art, game & sound design)

http://simplici7y.com/items/susannah-windows
